import * as loglevel from 'loglevel';

declare const log: log.LogContext;
export = log;

declare namespace log {

	type LogMethods = 'trace' | 'debug' | 'info' | 'warn' | 'error';

	type LogTimestamp = string | Date | number;

	type LogMessages = any[];

	interface FormatArg {

		/**
		 * 原始的 logger name
		 */
		rawName: string,

		/**
		 * 经过 withName 方法过滤后的 logger name
		 */
		name: string,

		/**
		 * 原始的 log method
		 */
		rawLevel: LogMethods,

		/**
		 * 经过 withLevel 方法过滤后的 level
		 */
		level: string,

		/**
		 * 经过 date 方法生成时间戳
		 */
		date: LogTimestamp;

		/**
		 * 实际执行 LogMethods 请求的参数
		 */
		args: LogMessages;
	}

	interface LogConfig {

		/**
		 * 当前 logger 等级
		 */
		level?: loglevel.LogLevelNumbers | loglevel.LogLevelDesc;

		/**
		 * 输出是否带有 logger name，可以是一个function，自行输出 logger name
		 */
		withName?: boolean | ((name: string) => string);

		/**
		 * 输出是否带有 level，可以是一个 function，自行输出 level 内容
		 */
		withLevel?: boolean | ((levelString: string, levelNumber: number) => string);

		/**
		 * 输出 log 的时间戳处理方法，必须指定为一个 function，否则不会输出时间戳
		 */
		date?: ((date: Date) => LogTimestamp);

		/**
		 * Logger 是否附带额外的特性
		 */
		features?: number;

		format?: ((formatArg: FormatArg) => LogMessages);

		handle?: ((...args: LogMessages) => void);
	}

	interface LogConfigs {
		loggerName?: LogConfig,
	}

	interface LogContext {

		canAll: number;

		canNone: number;

		canTable: number;

		canCount: number;

		canProfile: number;

		canTime: number;

		getLogger(name: string): Logger;

		setup(name: string, config?: LogConfig): LogContext;

		setup(name: LogConfigs): LogContext;

		methodFactory: loglevel.MethodFactory;

		readonly rootLogger: loglevel.RootLogger;
	}

	interface Logger extends loglevel.Logger {

		config: LogConfig;

		getLogger(name: string): Logger;

		setup(config: LogConfig): Logger;

		table(tabularData?: any, properties?: string[]): Logger;

		count(label?: string): Logger;

		countReset(label?: string): Logger;

		time(label?: string): Logger;

		timeEnd(label?: string): Logger;

		timeLog(label?: string, ...data: any[]): Logger;

		timeStamp(label?: string): Logger;

		profile(label?: string): Logger;

		profileEnd(label?: string): Logger;
	}
}
