# gitee-loglevel

gitee-loglevel 扩展，主要为了配置 babel-plugin-logger

扩展特性：

- 全局方法简化和扩展，支持 loggerName select

```js
const log = require('gitee-loglevel');
log.setup({
    default: {
        level: 'DEBUG',
        date() {
            return '20-10-21 12:31:44'        
        },
        withName: true,
        withLevel(level) {
            return level.toUpperCase();        
        },
        features: log.canCount | log.canTable | log.canProfile | log.canTime, // 额外支持的特性，默认为关闭，调用相关方法无任何输出
        // features 默认为 canNone，对应的是 count|table|profile|time 几个方法
    },
    test: {
        // 默认会合并 default 的设置
        level: 'WARN',
        format({rawName, name, rawLevel, level, date, message}) {
            return message; // 请返回一个数组，message 也是以数组形式
        },
        handle(...args) { // 只针对 trace|debug|info|warn|error
            console.log(...args)
        },
        features: log.canNone // log.canAll = log.canCount | log.canTable | log.canProfile | log.canTime
    }
});
const logger = log.getLogger('test.a.b'); // => 这会匹配到 test 的配置

```

- 在 loglevel 的基础上，增加几个常用 console 的方法：

```js
const log = require('gitee-loglevel');
const logger = log.getLogger('hello');

logger.table({a: 'a'});
logger.profile('a');
logger.profileEnd('a');
logger.time('b');
logger.timeEnd('b');
logger.count('c');
logger.countReset('c');
```

- 默认不带 date 输出，但是给 format 的方法传的消息结构带有 date 对象

- [loglevel 文档](https://github.com/pimterry/loglevel)